package app.dao.impl;

import app.model.domain.Toppings;
import app.model.pizzas.Ingredient;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class IngredientDAOTest {

    private static final double PRICE = 2.0;
    private static final Long ID = 1L;
    @Mock
    private Connection c;
    @Mock
    private PreparedStatement stmt;
    @Mock
    private ResultSet rs;

    Ingredient ingredient = new Ingredient();

    @Before
    public void setUp() throws Exception {
        when(c.prepareStatement(any(String.class))).thenReturn(stmt);
        when(c.createStatement()).thenReturn(stmt);

        ingredient.setId(ID);
        ingredient.setTopping(Toppings.CHEESE);
        ingredient.setPrice(PRICE);
        when((rs.next())).thenReturn(true,false);
        when(rs.getLong("id")).thenReturn(ingredient.getId());
        when(rs.getDouble("price")).thenReturn(ingredient.getPrice());
        when(rs.getString("topping_type")).thenReturn(ingredient.getTopping().getName());
        when(stmt.executeQuery(any(String.class))).thenReturn(rs);
        when(stmt.executeQuery()).thenReturn(rs);
    }

    @Test
    public void findAll() {
        assertEquals(ingredient.getPrice(), new IngredientDAOImpl(c).findAll().get(0).getPrice(), 0.01);
    }

    @Test
    public void findById() {
        assertEquals(ingredient.getPrice(), new IngredientDAOImpl(c).findById(ID).getPrice(), 0.01);
    }
}