package app.dao.impl;

import app.dao.CrustDAO;
import app.dao.SauceDAO;
import app.model.domain.CrustType;
import app.model.domain.PizzaType;
import app.model.domain.SauceType;
import app.model.domain.Toppings;
import app.model.orders.OrderItem;
import app.model.pizzas.Crust;
import app.model.pizzas.Ingredient;
import app.model.pizzas.Sauce;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Arrays;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class OrderItemDAOTest {

    private static final double PRICE = 2.0;
    private static final Long ID = 1L;

    @Mock
    private Connection c;
    @Mock
    private PreparedStatement stmt;
    @Mock
    private ResultSet rs;
    @Mock
    private CrustDAO crustDAO;
    @Mock
    private SauceDAO sauceDAO;

    Ingredient ingredient = new Ingredient();
    OrderItem orderItem = new OrderItem();

    @Before
    public void setUp() throws Exception {
        when(c.prepareStatement(any(String.class),anyInt())).thenReturn(stmt);
        when(c.prepareStatement(any(String.class))).thenReturn(stmt);
        when(c.createStatement()).thenReturn(stmt);

        ingredient.setId(ID);
        ingredient.setTopping(Toppings.CHEESE);
        ingredient.setPrice(PRICE);

        orderItem.setId(ID);
        orderItem.setIngredients(Arrays.asList(ingredient));
        orderItem.setCount(2);
        orderItem.setCrust(new Crust(1L,1,CrustType.THICK));
        orderItem.setOrderId(1L);
        orderItem.setSauce(new Sauce(1L,1, SauceType.PESTO));
        orderItem.setType(PizzaType.PEPPERONI);

        when((stmt.getGeneratedKeys())).thenReturn(rs);
        when((rs.next())).thenReturn(true,false);
        when((rs.getLong(1))).thenReturn(ID);
        when(rs.getLong("id")).thenReturn(ingredient.getId());
        when(rs.getDouble("price")).thenReturn(ingredient.getPrice());
        when(rs.getString("topping_type")).thenReturn(ingredient.getTopping().getName());
        when(stmt.executeQuery(any(String.class))).thenReturn(rs);
        when(stmt.executeQuery()).thenReturn(rs);
        when(stmt.executeUpdate()).thenReturn(ID.intValue());
    }

    @Test
    public void save() {
        assertEquals(ID, new OrderItemDAOImpl(c,crustDAO,sauceDAO).save(orderItem).getId());
    }

    @Test
    public void update() {
        assertTrue( new OrderItemDAOImpl(c,crustDAO,sauceDAO).update(orderItem));
    }

    @Test
    public void delete() {
        assertEquals(ID, new OrderItemDAOImpl(c,crustDAO,sauceDAO).delete(orderItem).getId());
    }

    @Test
    public void findByOrderItemId() {
        assertEquals(orderItem.getId(), new OrderItemDAOImpl(c,crustDAO,sauceDAO).findByOrderItemId(ID).getId());
    }
}