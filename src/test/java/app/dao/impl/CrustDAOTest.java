package app.dao.impl;

import app.model.domain.CrustType;
import app.model.pizzas.Crust;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CrustDAOTest {

    private static final double PRICE = 2.0;
    private static final Long ID = 1L;
    @Mock
    private Connection c;
    @Mock
    private PreparedStatement stmt;
    @Mock
    private ResultSet rs;

    Crust crust = new Crust();


    @Before
    public void setUp() throws Exception {
        when(c.prepareStatement(any(String.class))).thenReturn(stmt);
        when(c.createStatement()).thenReturn(stmt);

        crust.setId(ID);
        crust.setCrustType(CrustType.THICK);
        crust.setPrice(PRICE);
        when((rs.next())).thenReturn(true,false);
        when(rs.getLong("id")).thenReturn(crust.getId());
        when(rs.getDouble("price")).thenReturn(crust.getPrice());
        when(rs.getString("crust_type")).thenReturn(crust.getCrustType().getName());
        when(stmt.executeQuery(any(String.class))).thenReturn(rs);
        when(stmt.executeQuery()).thenReturn(rs);
    }

    @Test
    public void findAll() {
        assertEquals(crust.getPrice(), new CrustDAOImpl(c).findAll().get(0).getPrice(), 0.01);
    }

    @Test
    public void findById() {
        assertEquals(crust.getPrice(), new CrustDAOImpl(c).findById(ID).getPrice(), 0.01);
    }
}