package app.services.impl;

import app.dao.IngredientDAO;
import app.model.pizzas.Ingredient;
import app.services.IngredientService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class IngredientServiceTest {

    @Mock
    private IngredientDAO ingredientDAO;

    private IngredientService ingredientService;

    private Ingredient ingredient;

    @Before
    public void setUp() throws Exception {
        ingredientService = new IngredientServiceImpl(ingredientDAO);
        ingredient = new Ingredient();
    }

    @Test
    public void getAllIngredients() {
        Mockito.when(this.ingredientDAO.findAll()).thenReturn(Arrays.asList(ingredient));
        assertEquals(Arrays.asList(ingredient), this.ingredientService.getAllIngredients());
    }
}