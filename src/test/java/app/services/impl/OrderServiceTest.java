package app.services.impl;

import app.dao.OrderDAO;
import app.model.Order;
import app.model.pizzas.Crust;
import app.services.OrderService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class OrderServiceTest {

    private static final Long ID = 1L;
    @Mock
    private OrderDAO orderDAO;

    @Captor
    private ArgumentCaptor<Long> longArgumentCaptor;

    private OrderService orderService;

    private Order order;

    @Before
    public void setUp() throws Exception {
        orderService = new OrderServiceImpl(orderDAO);
        order = new Order();
    }

    @Test
    public void saveOrder() {
        Mockito.when(this.orderDAO.create(order)).thenReturn(order);
        assertEquals(order, this.orderService.saveOrder(order));
    }

    @Test
    public void updateOrder() {
        Mockito.when(this.orderDAO.update(order)).thenReturn(order);
        assertEquals(order, this.orderService.updateOrder(order));
    }

    @Test
    public void deleteOrder() {
        this.orderService.deleteOrder(ID);
        Mockito.verify(this.orderDAO).delete(longArgumentCaptor.capture());
        assertEquals(ID, longArgumentCaptor.getValue());
    }
}