package app.services.impl;

import app.dao.CrustDAO;
import app.model.pizzas.Crust;
import app.services.CrustService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class CrustServiceTest {

    @Mock
    private CrustDAO crustDAO;

    private CrustService crustService;

    private Crust crust;

    @Before
    public void setUp() throws Exception {
        crustService = new CrustServiceImpl(crustDAO);
        crust = new Crust();
    }

    @Test
    public void getAllCrusts() {
        Mockito.when(this.crustDAO.findAll()).thenReturn(Arrays.asList(crust));
        assertEquals(Arrays.asList(crust), this.crustService.getAllCrusts());
    }
}