package app.services.impl;

import app.dao.SauceDAO;
import app.model.pizzas.Sauce;
import app.services.SauceService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class SauceServiceImplTest {
    @Mock
    private SauceDAO sauceDAO;

    private SauceService sauceService;

    private Sauce sauce;

    @Before
    public void setUp() throws Exception {
        sauceService = new SauceServiceImpl(sauceDAO);
        sauce = new Sauce();
    }

    @Test
    public void getAllSauces() {
        Mockito.when(this.sauceDAO.findAll()).thenReturn(Arrays.asList(sauce));
        assertEquals(Arrays.asList(sauce), this.sauceService.getAllSauces());
    }
}