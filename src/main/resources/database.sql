CREATE TABLE `crust` (
  `id` INTEGER NOT NULL AUTO_INCREMENT,
  `price` DOUBLE NOT NULL DEFAULT 0,
  `crust_type` VARCHAR(20) NOT NULL UNIQUE,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB
CHECKSUM=0
DELAY_KEY_WRITE=0
PACK_KEYS=0
AUTO_INCREMENT=0
AVG_ROW_LENGTH=0
MIN_ROWS=0
MAX_ROWS=0
ROW_FORMAT=DEFAULT
KEY_BLOCK_SIZE=0;

CREATE TABLE `ingredient` (
  `id` INTEGER NOT NULL AUTO_INCREMENT,
  `price` DOUBLE NOT NULL DEFAULT 0,
  `topping` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB
CHECKSUM=0
DELAY_KEY_WRITE=0
PACK_KEYS=0
AUTO_INCREMENT=0
AVG_ROW_LENGTH=0
MIN_ROWS=0
MAX_ROWS=0
ROW_FORMAT=DEFAULT
KEY_BLOCK_SIZE=0;

CREATE TABLE `order` (
  `id` INTEGER NOT NULL AUTO_INCREMENT,
  `description` VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB
CHECKSUM=0
DELAY_KEY_WRITE=0
PACK_KEYS=0
AUTO_INCREMENT=0
AVG_ROW_LENGTH=0
MIN_ROWS=0
MAX_ROWS=0
ROW_FORMAT=DEFAULT
KEY_BLOCK_SIZE=0;

CREATE TABLE `order_item` (
  `id` INTEGER NOT NULL AUTO_INCREMENT,
  `count` INTEGER NOT NULL DEFAULT 1,
  `crust_id` INTEGER NOT NULL,
  `sauce_id` INTEGER NOT NULL,
  `pizza_type` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB
CHECKSUM=0
DELAY_KEY_WRITE=0
PACK_KEYS=0
AUTO_INCREMENT=0
AVG_ROW_LENGTH=0
MIN_ROWS=0
MAX_ROWS=0
ROW_FORMAT=DEFAULT
KEY_BLOCK_SIZE=0;

CREATE TABLE `sauce` (
  `id` INTEGER NOT NULL AUTO_INCREMENT,
  `price` DOUBLE NOT NULL DEFAULT 0,
  `sauce_type` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB
CHECKSUM=0
DELAY_KEY_WRITE=0
PACK_KEYS=0
AUTO_INCREMENT=0
AVG_ROW_LENGTH=0
MIN_ROWS=0
MAX_ROWS=0
ROW_FORMAT=DEFAULT
KEY_BLOCK_SIZE=0;

CREATE TABLE `order_to_order_item` (
  `order_id` INTEGER DEFAULT NULL,
  `order_item_id` INTEGER DEFAULT NULL
) ENGINE=InnoDB
CHECKSUM=0
DELAY_KEY_WRITE=0
PACK_KEYS=0
AUTO_INCREMENT=0
AVG_ROW_LENGTH=0
MIN_ROWS=0
MAX_ROWS=0
ROW_FORMAT=DEFAULT
KEY_BLOCK_SIZE=0;

CREATE TABLE `ingredient_to_order_item` (
  `order_item_id` INTEGER DEFAULT NULL,
  `ingredient_id` INTEGER DEFAULT NULL
) ENGINE=InnoDB
CHECKSUM=0
DELAY_KEY_WRITE=0
PACK_KEYS=0
AUTO_INCREMENT=0
AVG_ROW_LENGTH=0
MIN_ROWS=0
MAX_ROWS=0
ROW_FORMAT=DEFAULT
KEY_BLOCK_SIZE=0;