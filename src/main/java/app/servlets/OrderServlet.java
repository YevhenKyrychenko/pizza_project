package app.servlets;

import app.model.Order;
import app.model.domain.PizzaType;
import app.services.CrustService;
import app.services.IngredientService;
import app.services.OrderService;
import app.services.SauceService;
import app.services.impl.CrustServiceImpl;
import app.services.impl.IngredientServiceImpl;
import app.services.impl.OrderServiceImpl;
import app.services.impl.SauceServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class OrderServlet extends HttpServlet {

    private OrderService orderService = new OrderServiceImpl();
    private SauceService sauceService = new SauceServiceImpl();
    private IngredientService ingredientService = new IngredientServiceImpl();
    private CrustService crustService = new CrustServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("sauces", sauceService.getAllSauces()/*Arrays.asList(new Sauce(1l,10.0, SauceType.PESTO),new Sauce(1l,10.0, SauceType.MARINARA))*/);
        req.setAttribute("ingredients", ingredientService.getAllIngredients() /*Arrays.asList(new Ingredient(1l,11.0, Toppings.CHEESE))*/);
        req.setAttribute("crusts", crustService.getAllCrusts()/*Arrays.asList(new Crust(1l,9.0, CrustType.THICK))*/);
        req.setAttribute("types", PizzaType.getAllValues());

        RequestDispatcher requestDispatcher = req.getRequestDispatcher("views/order.jsp");
        requestDispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println(req.getParameter("order"));
        ObjectMapper mapper = new ObjectMapper();
        Order order = mapper.readValue(req.getParameter("order"), Order.class);
        order.setDescription("");
        this.orderService.saveOrder(order);

        req.setAttribute("order", order);
        doGet(req, resp);
    }
}
