package app.model.orders;

import app.model.domain.PizzaType;
import app.model.pizzas.Crust;
import app.model.pizzas.Ingredient;
import app.model.pizzas.Sauce;

import java.util.List;
import java.util.stream.Collectors;

public class OrderItem {
    private Long id;
    private Long orderId;
    private PizzaType type;
    private int count;
    private List<Ingredient> ingredients;
    private Sauce sauce;
    private Crust crust;

    public OrderItem() {
    }

    public OrderItem(Long id, Long orderId, PizzaType type, int count, List<Ingredient> ingredients, Sauce sauce, Crust crust) {
        this.id = id;
        this.orderId = orderId;
        this.type = type;
        this.count = count;
        this.ingredients = ingredients;
        this.sauce = sauce;
        this.crust = crust;
    }

    public String getIngredientsIds(){
        return ingredients.stream().map(i -> i.getId().toString()).collect(Collectors.joining( "," ));
    }

    public PizzaType getType() {
        return type;
    }

    public int getCount() {
        return count;
    }

    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    public Sauce getSauce() {
        return sauce;
    }

    public Crust getCrust() {
        return crust;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public void setType(PizzaType type) {
        this.type = type;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public void setIngredients(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    public void setSauce(Sauce sauce) {
        this.sauce = sauce;
    }

    public void setCrust(Crust crust) {
        this.crust = crust;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
