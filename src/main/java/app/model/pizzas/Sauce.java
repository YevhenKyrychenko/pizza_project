package app.model.pizzas;

import app.model.domain.SauceType;

public class Sauce {
    private Long id;
    private double price;
    private SauceType sauceType;

    public Sauce() {
    }

    public Sauce(Long id, double price, SauceType sauceType) {
        this.id = id;
        this.price = price;
        this.sauceType = sauceType;
    }

    public Sauce(SauceType sauceType, double price) {
        this.price = price;
        this.sauceType = sauceType;
    }

    public double getPrice() {
        return price;
    }

    public SauceType getSauceType() {
        return sauceType;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setSauceType(SauceType sauceType) {
        this.sauceType = sauceType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Sauce{" +
                "price=" + price +
                ", sauceType=" + sauceType +
                '}';
    }
}
