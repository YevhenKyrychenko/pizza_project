package app.model.pizzas;

import app.model.domain.Toppings;

public class Ingredient {
    private Long id;
    private double price;
    private Toppings topping;

    public Ingredient() {
    }

    public Ingredient(Long id, double price, Toppings topping) {
        this.id = id;
        this.price = price;
        this.topping = topping;
    }

    public Ingredient(Toppings topping, double price) {
        this.price = price;
        this.topping = topping;
    }

    public double getPrice() {
        return price;
    }

    public Toppings getTopping() {
        return topping;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setTopping(Toppings topping) {
        this.topping = topping;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Ingredient{" +
                "price=" + price +
                ", topping=" + topping +
                '}';
    }
}
