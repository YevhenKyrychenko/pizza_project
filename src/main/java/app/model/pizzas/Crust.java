package app.model.pizzas;

import app.model.domain.CrustType;

public class Crust {
    private Long id;
    private double price;
    private CrustType crustType;

    public Crust() {
    }

    public Crust(Long id, double price, CrustType crustType) {
        this.id = id;
        this.price = price;
        this.crustType = crustType;
    }

    public Crust(double thinPrice, CrustType thin) {
        this.price = price;
        this.crustType = crustType;
    }

    public double getPrice() {
        return price;
    }

    public CrustType getCrustType() {
        return crustType;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setCrustType(CrustType crustType) {
        this.crustType = crustType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Crust{" +
                "price=" + price +
                ", crustType=" + crustType +
                '}';
    }


}
