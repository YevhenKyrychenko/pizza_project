package app.model.domain;

import java.util.Arrays;

public enum SauceType {
    MARINARA("MARINARA"),
    PESTO("PESTO"),
    PLUM_TOMATO("PLUM_TOMATO");

    private String name;

    SauceType(String name) {
        this.name = name;
    }

    public static SauceType findByName(String name){
        return Arrays.stream(values())
                .filter(gender -> gender.getName().equals(name))
                .findFirst()
                .orElse(MARINARA);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
