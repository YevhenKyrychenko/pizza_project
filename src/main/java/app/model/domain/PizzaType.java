package app.model.domain;

import java.util.Arrays;
import java.util.List;

public enum PizzaType {
    CUSTOM("CUSTOM"),
    PEPPERONI("PEPPERONI"),
    CHEESE("CHEESE");

    private String name;

    public static PizzaType findByName(String name){
        return Arrays.stream(values())
                .filter(gender -> gender.getName().equals(name))
                .findFirst()
                .orElse(PEPPERONI);
    }

    public static List<PizzaType> getAllValues() {
        return Arrays.asList(CUSTOM,PEPPERONI,CHEESE);
    }


    PizzaType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
