package app.model.domain;

import java.util.Arrays;

public enum CrustType {
    THICK("THICK"), THIN("THIN");

    private String name;

    CrustType(String name) {
        this.name = name;
    }

    public static CrustType findByName(String name){
        return Arrays.stream(values())
                .filter(gender -> gender.getName().equals(name))
                .findFirst()
                .orElse(THICK);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
