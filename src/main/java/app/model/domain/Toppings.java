package app.model.domain;

import java.util.Arrays;

public enum Toppings {
    CHEESE("CHEESE"),
    SALAMI("SALAMI"),
    PEPPERONI("PEPPERONI"),
    MUSHROOMS("MUSHROOMS"),
    ONIONS("ONIONS"),
    SAUSAGE("SAUSAGE"),
    BACON("BACON"),
    PINEAPPLE("PINEAPPLE"),
    PEPPERS("PEPPERS");

    private String name;

    Toppings(String name) {
        this.name = name;
    }

    public static Toppings findByName(String name){
        return Arrays.stream(values())
                .filter(gender -> gender.getName().equals(name))
                .findFirst()
                .orElse(CHEESE);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
