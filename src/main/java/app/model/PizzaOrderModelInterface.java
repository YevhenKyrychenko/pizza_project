package app.model;
import app.model.pizzas.Pizza;

import java.util.List;

public interface PizzaOrderModelInterface {
    List<Pizza> preparePizzas(Order order);
}
