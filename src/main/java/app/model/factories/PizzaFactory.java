package app.model.factories;

import app.model.pizzas.Crust;
import app.model.pizzas.Ingredient;
import app.model.pizzas.Pizza;
import app.model.pizzas.Sauce;

import java.util.List;

public interface PizzaFactory {
    Pizza bakePepperoni();
    Pizza bakeCustomPizza(List<Ingredient> ingredients, Sauce sauce, Crust dough);
}
