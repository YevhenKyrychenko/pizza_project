package app.model.factories;

import app.model.pizzas.Crust;
import app.model.pizzas.Ingredient;
import app.model.pizzas.Pizza;
import app.model.pizzas.Sauce;

import java.util.ArrayList;
import java.util.List;

public class PizzaFactoryImpl implements PizzaFactory {
    private static List<Ingredient> PEPPERONI_INGREDIENTS = new ArrayList<>();

    static {
        PEPPERONI_INGREDIENTS.add(IngredientsFactory.createCheeseIngredient());
        PEPPERONI_INGREDIENTS.add(IngredientsFactory.createPepperoniIngredient());
        PEPPERONI_INGREDIENTS.add(IngredientsFactory.createPeppersIngredient());
    }

    @Override
    public Pizza bakePepperoni(){
        return new Pizza(PEPPERONI_INGREDIENTS, SauceFactory.createPlumTomatoSauce(), CrustFactory.createThickCrust());
    }

    @Override
    public Pizza bakeCustomPizza(List<Ingredient> ingredients, Sauce sauce, Crust dough) {
        return new Pizza(ingredients, sauce, dough);
    }
}
