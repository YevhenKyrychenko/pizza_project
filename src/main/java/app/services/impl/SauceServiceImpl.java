package app.services.impl;

import app.dao.SauceDAO;
import app.dao.impl.SauceDAOImpl;
import app.model.pizzas.Sauce;
import app.services.SauceService;

import java.util.List;

public class SauceServiceImpl implements SauceService {

    private SauceDAO sauceDAO;

    public SauceServiceImpl(SauceDAO sauceDAO) {
        this.sauceDAO = sauceDAO;
    }

    public SauceServiceImpl() {
        sauceDAO = new SauceDAOImpl();
    }

    @Override
    public List<Sauce> getAllSauces() {
        return this.sauceDAO.findAll();
    }
}
