package app.services.impl;

import app.dao.IngredientDAO;
import app.dao.impl.IngredientDAOImpl;
import app.model.pizzas.Ingredient;
import app.services.IngredientService;

import java.util.List;

public class IngredientServiceImpl implements IngredientService {

    private IngredientDAO ingredientDAO;

    public IngredientServiceImpl() {
        ingredientDAO = new IngredientDAOImpl();
    }

    public IngredientServiceImpl(IngredientDAO ingredientDAO) {
        this.ingredientDAO = ingredientDAO;
    }

    @Override
    public List<Ingredient> getAllIngredients() {
        return this.ingredientDAO.findAll();
    }
}
