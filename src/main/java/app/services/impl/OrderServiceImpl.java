package app.services.impl;

import app.dao.OrderDAO;
import app.dao.impl.OrderDAOImpl;
import app.model.Order;
import app.services.OrderService;

public class OrderServiceImpl implements OrderService {

    private OrderDAO orderDAO;

    public OrderServiceImpl(OrderDAO orderDAO) {
        this.orderDAO = orderDAO;
    }

    public OrderServiceImpl() {
        orderDAO = new OrderDAOImpl();
    }

    @Override
    public Order saveOrder(Order order) {
        return this.orderDAO.create(order);
    }

    @Override
    public Order updateOrder(Order order) {
        return this.orderDAO.update(order);
    }

    @Override
    public void deleteOrder(Long id) {
        this.orderDAO.delete(id);
    }
}
