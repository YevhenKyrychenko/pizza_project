package app.services.impl;

import app.dao.CrustDAO;
import app.dao.impl.CrustDAOImpl;
import app.model.pizzas.Crust;
import app.services.CrustService;

import java.util.List;

public class CrustServiceImpl implements CrustService {

    private CrustDAO crustDAO;


    public CrustServiceImpl() {
        crustDAO = new CrustDAOImpl();
    }

    public CrustServiceImpl(CrustDAO crustDAO) {
        this.crustDAO = crustDAO;
    }

    @Override
    public List<Crust> getAllCrusts() {
        return this.crustDAO.findAll();
    }
}
