package app.services;

import app.model.pizzas.Sauce;

import java.util.List;

public interface SauceService {

    List<Sauce> getAllSauces();
}
