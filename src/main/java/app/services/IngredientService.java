package app.services;

import app.model.pizzas.Ingredient;

import java.util.List;

public interface IngredientService {

    List<Ingredient> getAllIngredients();
}
