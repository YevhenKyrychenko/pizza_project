package app.services;

import app.model.pizzas.Crust;

import java.util.List;

public interface CrustService {

    List<Crust> getAllCrusts();
}
