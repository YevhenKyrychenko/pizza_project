package app.services;

import app.model.Order;

public interface OrderService {

    Order saveOrder(Order order);

    Order updateOrder(Order order);

    void deleteOrder(Long id);
}
