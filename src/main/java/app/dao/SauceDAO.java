package app.dao;

import app.model.pizzas.Sauce;

import java.util.List;

public interface SauceDAO {

    List<Sauce> findAll();

    Sauce findById(Long id);
}
