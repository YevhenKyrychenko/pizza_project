package app.dao;

import app.model.pizzas.Crust;

import java.util.List;

public interface CrustDAO {

    List<Crust> findAll();

    Crust findById(Long id);
}
