package app.dao.impl;

import app.dao.Connector;
import app.dao.CrustDAO;
import app.model.domain.CrustType;
import app.model.pizzas.Crust;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CrustDAOImpl implements CrustDAO {

    private Connection connection;

    public CrustDAOImpl() {
        this.connection = Connector.getConnection();
    }

    public CrustDAOImpl(Connection connection) {
        this.connection = connection;
    }

    @Override
    public List<Crust> findAll() {
        try {
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM crust");
            List<Crust> crusts = new ArrayList();
            while(rs.next())
            {
                Crust crust = extractCrustFromResultSet(rs);
                crusts.add(crust);
            }
            return crusts;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public Crust findById(Long id) {
        try {
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM crust WHERE id=?");
            ps.setInt(1, id.intValue());
            ResultSet rs = ps.executeQuery();
            if(rs.next())
            {
                return extractCrustFromResultSet(rs);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }


    private Crust extractCrustFromResultSet(ResultSet rs) throws SQLException {
        Crust crust = new Crust();
        crust.setId(rs.getLong("id"));
        crust.setPrice( rs.getDouble("price") );
        crust.setCrustType(CrustType.findByName(rs.getString("crust_type")));
        return crust;
    }
}
