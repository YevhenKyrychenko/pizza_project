package app.dao.impl;

import app.dao.Connector;
import app.dao.IngredientDAO;
import app.model.domain.Toppings;
import app.model.pizzas.Ingredient;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class IngredientDAOImpl implements IngredientDAO {

    private Connection connection;

    public IngredientDAOImpl(Connection connection) {
        this.connection = connection;
    }

    public IngredientDAOImpl() {
        connection = Connector.getConnection();
    }

    @Override
    public List<Ingredient> findAll() {
        try {
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM ingredient");
            List<Ingredient> ingredients = new ArrayList<>();
            while(rs.next())
            {
                Ingredient ingredient = extractIngredientFromResultSet(rs);
                ingredients.add(ingredient);
            }
            return ingredients;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public Ingredient findById(Long id) {
        try {
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM ingredient WHERE id=?");
            ps.setInt(1, id.intValue());
            ResultSet rs = ps.executeQuery();
            if(rs.next())
            {
                return extractIngredientFromResultSet(rs);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    private Ingredient extractIngredientFromResultSet(ResultSet rs) throws SQLException {
        Ingredient ingredient = new Ingredient();
        ingredient.setId(rs.getLong("id"));
        ingredient.setPrice( rs.getDouble("price") );
        ingredient.setTopping(Toppings.findByName(rs.getString("topping")));
        return ingredient;
    }
}
