package app.dao.impl;

import app.dao.Connector;
import app.dao.CrustDAO;
import app.dao.OrderItemDAO;
import app.dao.SauceDAO;
import app.model.domain.PizzaType;
import app.model.domain.Toppings;
import app.model.orders.OrderItem;
import app.model.pizzas.Ingredient;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class OrderItemDAOImpl implements OrderItemDAO {

    private Connection connection;
    private CrustDAO crustDAO = new CrustDAOImpl();
    private SauceDAO sauceDAO = new SauceDAOImpl();

    public OrderItemDAOImpl(Connection connection, CrustDAO crustDAO, SauceDAO sauceDAO) {
        this.connection = connection;
        this.crustDAO = crustDAO;
        this.sauceDAO = sauceDAO;
    }

    public OrderItemDAOImpl() {
        connection = Connector.getConnection();
    }

    @Override
    public OrderItem save(OrderItem item) {
        try {
            PreparedStatement cs = connection.prepareStatement("INSERT INTO order_item(`count`, crust_id, sauce_id, pizza_type) VALUES ( ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
            cs.setInt(1, item.getCount());
            cs.setLong(2, item.getCrust().getId());
            cs.setLong(3, item.getSauce().getId());
            cs.setString(4, item.getType().toString());
            int i = cs.executeUpdate();
            if(i == 1) {
                ResultSet generatedKeys = cs.getGeneratedKeys();
                if(generatedKeys.next()) {
                    item.setId(generatedKeys.getLong(1));
                    item.getIngredients().forEach(o -> saveIngredient(o, item.getId()));
                    return item;
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean update(OrderItem item) {
        try {
            PreparedStatement ps = connection.prepareStatement("UPDATE order_item SET `count`=?, crust_id=?, sauce_id=?, type_id=? WHERE id=?");
            ps.setInt(1, item.getCount());
            ps.setLong(2, item.getCrust().getId());
            ps.setLong(3, item.getSauce().getId());
            ps.setString(4, item.getType().toString());
            ps.setInt(5, item.getOrderId().intValue());
            int i = ps.executeUpdate();
            deleteIngredients(item);
            item.getIngredients().forEach(o -> saveIngredient(o, item.getId()));
            return true;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return false;
    }

    @Override
    public OrderItem delete(OrderItem item) {
        try {
            Statement stmt = connection.createStatement();
            int i = stmt.executeUpdate("DELETE FROM order_item WHERE id=" + item.getId());
            deleteIngredients(item);
            return item;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public OrderItem findByOrderItemId(Long orderItemId) {
        OrderItem item = new OrderItem();
        try {
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM order_item WHERE id=?");
            ps.setInt(1, orderItemId.intValue());
            ResultSet rs = ps.executeQuery();
            try {
                while (rs.next()) {
                    item.setId(rs.getLong("id"));
                    item.setOrderId((long) rs.getInt("order_id"));
                    item.setCount(rs.getInt("count_id"));
                    item.setCrust(this.crustDAO.findById(rs.getLong("crust_id")));
                    item.setIngredients(getIngredientsByOrderItemId(orderItemId));
                    item.setSauce(this.sauceDAO.findById(rs.getLong("sauce_id")));
                    item.setType(PizzaType.findByName(rs.getString("type_id")));
                }
            } finally {
                rs.close();
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return item;
    }

    private List<Ingredient> getIngredientsByOrderItemId(Long id) {
        try {
            Statement stmt = connection.createStatement();
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM ingredient_to_order_item WHERE order_item_id=?");
            ps.setInt(1, id.intValue());
            ResultSet rs = ps.executeQuery();
            List<Ingredient> ingredients = new ArrayList();
            while(rs.next())
            {
                Ingredient ingredient = extractIngredientFromResultSet(rs);
                ingredients.add(ingredient);
            }
            return ingredients;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    private Ingredient extractIngredientFromResultSet(ResultSet rs) throws SQLException {
        Ingredient ingredient = new Ingredient();
        ingredient.setId(rs.getLong("id"));
        ingredient.setPrice( rs.getDouble("price") );
        ingredient.setTopping(Toppings.findByName(rs.getString("topping")));
        return ingredient;
    }

    private Ingredient saveIngredient(Ingredient ingredient, Long orderItemId){
        try {
            PreparedStatement ps = connection.prepareStatement("INSERT INTO ingredient_to_order_item(ingredient_id, order_item_id) VALUES ( ?, ?)");
            ps.setLong(1, ingredient.getId());
            ps.setLong(2, orderItemId);
            int i = ps.executeUpdate();
            if(i == 1) {
                return ingredient;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    private void deleteIngredients(OrderItem item) {
        try {
            Statement stmt = connection.createStatement();
            int i = stmt.executeUpdate("DELETE FROM ingredient_to_order_item WHERE order_item_id=" + item.getId());
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
