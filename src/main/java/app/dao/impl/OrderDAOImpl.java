package app.dao.impl;

import app.dao.Connector;
import app.dao.OrderDAO;
import app.dao.OrderItemDAO;
import app.model.Order;
import app.model.orders.OrderItem;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class OrderDAOImpl implements OrderDAO {

    private OrderItemDAO orderItemDAO = new OrderItemDAOImpl();

    private Connection connection;

    public OrderDAOImpl(OrderItemDAO orderItemDAO, Connection connection) {
        this.orderItemDAO = orderItemDAO;
        this.connection = connection;
    }

    public OrderDAOImpl() {
        connection = Connector.getConnection();
    }

    @Override
    public Order create(Order order) {
        try {
            PreparedStatement cs = connection.prepareStatement("INSERT INTO `order` (description) VALUES (?)", Statement.RETURN_GENERATED_KEYS);
            cs.setString(1, order.getDescription());
            int i = cs.executeUpdate();
            if(i == 1) {
                ResultSet generatedKeys = cs.getGeneratedKeys();
                if(generatedKeys.next()) {
                    order.setId(generatedKeys.getLong(1));
                    order.getOrderItems().forEach(o -> {
                        saveOrderItem(this.orderItemDAO.save(o), order.getId());
                    });
                }
                return order;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public Order update(Order order) {
        try {
            PreparedStatement ps = connection.prepareStatement("UPDATE order SET description=? WHERE id=?");
            ps.setString(1, order.getDescription());
            ps.setInt(2, order.getId().intValue());
            int i = ps.executeUpdate();
            deleteOrderItems(order);
            order.getOrderItems().forEach(o -> {
                this.orderItemDAO.delete(o);
                saveOrderItem(o, order.getId());
                this.orderItemDAO.save(o);
            });
            return this.findById(order.getId());
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public Long delete(Long orderId) {
        try {
            Statement stmt = connection.createStatement();
            int i = stmt.executeUpdate("DELETE FROM order WHERE id=" + orderId);
            Order order = this.findById(orderId);
            deleteOrderItems(order);
            order.getOrderItems().forEach(o -> this.orderItemDAO.delete(o));
            return orderId;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public Order findById(Long id) {
        Order order = new Order();
        try {
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM order WHERE id=?");
            ps.setInt(1, id.intValue());
            ResultSet rs = ps.executeQuery();
            try {
                while (rs.next()) {
                    order.setId(rs.getLong("id"));
                    order.setDescription(rs.getString("description"));
                    order.setOrderItems(getOrderItemsByOrderId(id));
                }
            } finally {
                rs.close();
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return order;
    }

    private void saveOrderItem(OrderItem item, Long orderId) {
        try {
            PreparedStatement ps = connection.prepareStatement("INSERT INTO order_to_order_item(order_id, order_item_id) VALUES ( ?, ?)");
            ps.setLong(1, orderId);
            ps.setLong(2, item.getId());
            int i = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    private void deleteOrderItems(Order order) {
        try {
            Statement stmt = connection.createStatement();
            int i = stmt.executeUpdate("DELETE FROM order_to_order_item WHERE order_id=" + order.getId());
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    private List<OrderItem> getOrderItemsByOrderId(Long orderId) {
        try {
            Statement stmt = connection.createStatement();
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM order_to_order_item WHERE order_id=?");
            ps.setInt(1, orderId.intValue());
            ResultSet rs = ps.executeQuery();
            List<OrderItem> items = new ArrayList();
            while(rs.next())
            {
                OrderItem item = this.orderItemDAO.findByOrderItemId(rs.getLong("order_item_id"));
                items.add(item);
            }
            return items;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }
}
