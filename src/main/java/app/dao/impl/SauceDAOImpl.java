package app.dao.impl;

import app.dao.Connector;
import app.dao.SauceDAO;
import app.model.domain.SauceType;
import app.model.pizzas.Sauce;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SauceDAOImpl implements SauceDAO {

    private Connection connection;

    public SauceDAOImpl(Connection connection) {
        this.connection = connection;
    }

    public SauceDAOImpl() {
        connection = Connector.getConnection();
    }

    @Override
    public List<Sauce> findAll() {
        try {
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM sauce");
            List<Sauce> sauces = new ArrayList<>();
            while(rs.next())
            {
                Sauce sauce = extractSauceFromResultSet(rs);
                sauces.add(sauce);
            }
            return sauces;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public Sauce findById(Long id) {
        try {
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM sauce WHERE id=?");
            ps.setInt(1, id.intValue());
            ResultSet rs = ps.executeQuery();
            if(rs.next())
            {
                return extractSauceFromResultSet(rs);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    private Sauce extractSauceFromResultSet(ResultSet rs) throws SQLException {
        Sauce sauce = new Sauce();
        sauce.setId(rs.getLong("id"));
        sauce.setPrice( rs.getDouble("price") );
        sauce.setSauceType(SauceType.findByName(rs.getString("sauce_type")));
        return sauce;
    }
}
