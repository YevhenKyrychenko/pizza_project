package app.dao;

import app.model.orders.OrderItem;

public interface OrderItemDAO {
    OrderItem save(OrderItem item);

    boolean update(OrderItem item);

    OrderItem delete(OrderItem item);

    OrderItem findByOrderItemId(Long orderId);
}
