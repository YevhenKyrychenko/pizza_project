package app.dao;

import app.model.pizzas.Ingredient;

import java.util.List;

public interface IngredientDAO {

    List<Ingredient> findAll();

    Ingredient findById(Long id);
}
