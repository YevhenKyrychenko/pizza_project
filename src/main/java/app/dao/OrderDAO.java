package app.dao;

import app.model.Order;

public interface OrderDAO {
    Order create(Order order);

    Order update(Order order);

    Long delete(Long orderId);

    Order findById(Long id);
}
