
<%@ page import="java.util.List" %>
<%@ page import="app.model.pizzas.Sauce" %>
<%@ page import="app.model.pizzas.Ingredient" %>
<%@ page import="app.model.pizzas.Crust" %>
<%@ page import="app.model.domain.PizzaType" %><%--
  Created by IntelliJ IDEA.
  User: Jeka
  Date: 7/9/2020
  Time: 12:36 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Order</title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</head>

<body class="w3-light-grey">
<script>
    window.order = {
        orderItems: []
    };
    var addOrderItem = function() {
        var orderItemContainer = document.getElementById('order-item-container');
        var inputs = orderItemContainer.getElementsByTagName('input');
        var data = [...inputs].filter(input => ((input.type != 'checkbox'  && input.type != 'radio') || input.checked ))
            .map(input => ({type: input.type, name: input.name, value: input.value, checked: input.checked}));
        var orderItem = {
            ingredients: data.filter(obj => obj.name == 'ingredients').map(obj => ({id: obj.value})),
            sauce: data.filter(obj => obj.name == 'sauces').map(obj => ({id: obj.value}))[0],
            crust: data.filter(obj => obj.name == 'crusts').map(obj => ({id: obj.value}))[0],
            type: data.filter(obj => obj.name == 'types').map(obj => obj.value)[0],
            count: data.filter(obj => obj.name == 'count').map(obj => obj.value )[0],
        };
        window.order.orderItems.push(orderItem);
        document.getElementById('order-holder').value = JSON.stringify(window.order);
    };
</script>
<div class="w3-container w3-blue-grey w3-opacity w3-right-align">
    <h1>Order</h1>
</div>


<div id="order-item-container" class="w3-container w3-yellow">
    <%
        List<Sauce> sauces = (List<Sauce>) request.getAttribute("sauces");

        if (sauces != null && !sauces.isEmpty()) {
            out.println("<div><ul class=\"w3-ul\">");
            for (Sauce sauce : sauces) {
                out.println("<div><input class=\"w3-hover-sand\" type = \"radio\" name = \"sauces\" value ="+ sauce.getId() + " />" + sauce.getSauceType()+ "</div>");
            }
            out.println("</ul></div>");
        }
        List<Ingredient> ingredients = (List<Ingredient>) request.getAttribute("ingredients");

        if (ingredients != null && !ingredients.isEmpty()) {
            out.println("<div><ul class=\"w3-ul\">");
            for (Ingredient ingredient : ingredients) {
                out.println("<div><input class=\"w3-hover-sand\" type = \"checkbox\" name = \"ingredients\" value = " + ingredient.getId()+ " />" + ingredient.getTopping()+ "</div>");
            }
            out.println("</ul></div>");
        }
        List<Crust> crusts = (List<Crust>) request.getAttribute("crusts");

        if (crusts != null && !crusts.isEmpty()) {
            out.println("<div><ul class=\"w3-ul\">");
            for (Crust crust : crusts) {
                out.println("<div><input class=\"w3-hover-sand\" type = \"radio\" name = \"crusts\" value = " + crust.getId() + " />" + crust.getCrustType()+ "</div>");
            }
            out.println("</ul></div>");
        }
        List<PizzaType> types = (List<PizzaType>) request.getAttribute("types");

        if (types != null && !types.isEmpty()) {
            out.println("<div><ul class=\"w3-ul\">");
            for (PizzaType type : types) {
                out.println("<div><input class=\"w3-hover-sand\" type = \"radio\" name = \"types\" value = " + type.getName() + " />" + type.getName() + "</div>");
            }
            out.println("</ul></div>");
        }


    %>
    <label>Count:
        <input type="number" min="1" default = "1" name="count" class="w3-input w3-animate-input w3-border w3-round-large" style="width: 30%"><br />
    </label>
    <button type="submit" class="w3-btn w3-green w3-round-large w3-margin-bottom" onclick="addOrderItem()">Submit</button>
</div>
<div class="w3-container w3-grey w3-opacity w3-right-align w3-padding">
    <button class="w3-btn w3-round-large" onclick="location.href='/'">Back to main</button>
</div>
<form action="/order" method="POST">
    <input id="order-holder" type="hidden" name="order">
    <button type="submit" class="w3-btn w3-green w3-round-large w3-margin-bottom">Submit order</button>
</body>
</html>
